import "./App.css";
import HackerNews from "./tabs/HackerNews.js";
import StoryDetails from "./items/StoryDetails.js";
import React from "react";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	NavLink,
	Redirect,
} from "react-router-dom";

function App() {
	return (
		<Router>
			<div className="grid-container">
				<div className="center-container"></div>
				<div className="navbar">
					<NavLink exact to="/" className="y">
						Y
					</NavLink>
					<NavLink to="/news" className="hnlogo">
						Hacker News
					</NavLink>
					<NavLink to="/newest" className="navigation-item">
						new
					</NavLink>
					<span className="navigation-item">|</span>
					<NavLink to="/ask" className="navigation-item">
						ask
					</NavLink>
					<span className="navigation-item">|</span>
					<NavLink to="/show" className="navigation-item">
						show
					</NavLink>
					<span className="navigation-item">|</span>
					<NavLink to="/jobs" className="navigation-item">
						jobs
					</NavLink>
				</div>
				<div className="footer">
					<hr className="footerHr" />
				</div>
				<div className="body">
					<Switch>
						<Route exact path="/">
							<Redirect to="/news"></Redirect>
						</Route>
						<Route
							path="/news"
							render={(props) => (
								<HackerNews {...props} typeOfStory={"topstories"} key={"topstories"} />
							)}
						/>

						<Route
							path="/newest"
							render={(props) => (
								<HackerNews {...props} typeOfStory={"newstories"} key={"newstories"} />
							)}
						/>
						<Route
							path="/ask"
							render={(props) => (
								<HackerNews {...props} typeOfStory={"askstories"} key={"askstories"} />
							)}
						/>
						<Route
							path="/show"
							render={(props) => (
								<HackerNews
									{...props}
									typeOfStory={"showstories"}
									key={"showstories"}
								/>
							)}
						/>
						<Route
							path="/jobs"
							render={(props) => (
								<HackerNews {...props} typeOfStory={"jobstories"} key={"jobstories"} />
							)}
						/>
						<Route path="/item/:id">
							<StoryDetails></StoryDetails>
						</Route>
					</Switch>
				</div>
			</div>
		</Router>
	);
}

export default App;
