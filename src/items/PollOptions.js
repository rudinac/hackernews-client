import axios from "axios";
import {useState, useEffect} from "react";

const fetchItemDetails = async (url) => {
	const item = await axios(url);
	return item.data;
};

function fetchPollOptions(partIds) {
	const requests = partIds.map((id) =>
		fetchItemDetails(
			`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`
		)
	);
	return Promise.all(requests);
}

function PollOptions({partIds}) {
	const [options, setOptions] = useState([]);
	useEffect(() => {
		fetchPollOptions(partIds).then((fetchedDetails) =>
			setOptions(fetchedDetails)
		);
	}, [partIds]);
	return (
		<>
			{options &&
				options.map((option) => {
					return (
						<div className="pollopt-wrapper">
							<div className="comment-content">{option.text}</div>
							<div className="comment-header">{option.score}</div>
						</div>
					);
				})}
		</>
	);
}
export default PollOptions;
