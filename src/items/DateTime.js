import {format, formatDistanceToNowStrict, differenceInYears} from "date-fns";

function DateTime({seconds}) {
	function calculateDateAndTime() {
		if (differenceInYears(Date.now(), new Date(seconds * 1000)) >= 1)
			return "on " + format(new Date(seconds * 1000), "MMM d, yyyy");
		else
			return formatDistanceToNowStrict(new Date(seconds * 1000), {
				addSuffix: true,
			});
	}
	return <>{calculateDateAndTime()}</>;
}

export default DateTime;
