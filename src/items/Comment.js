import {useEffect, useState} from "react";
import DateTime from "./DateTime.js";
import axios from "axios";
import DOMPurify from "dompurify";
import "../Items.css";

async function fetchComment(url) {
	const comment = await axios(url);
	return comment.data;
}

function Comment({id}) {
	const [comment, setComment] = useState({kids: []});
	const [visible, setVisible] = useState(true);

	useEffect(() => {
		fetchComment(
			`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`
		).then((fetchedDetails) => setComment(() => fetchedDetails));
	}, [id]);

	function handleHide() {
		setVisible((visible) => !visible);
	}

	return (
		<div style={{paddingLeft: "30px"}}>
			{comment.by ? (
				<div className="comment-header">
					<span>{comment.by}</span>
					<span>
						{" "}
						<DateTime seconds={comment.time}></DateTime>
					</span>
					<span
						role="button"
						className="hide-button"
						aria-pressed="false"
						onClick={handleHide}
					>
						{!visible ? " [+] " : " [-] "}
					</span>
				</div>
			) : null}
			{comment && visible && (
				<div
					className="comment-content"
					dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(comment.text)}}
				></div>
			)}
			<div className={visible ? "comment-visible" : "comment-hidden"}>
				{comment.kids &&
					comment.kids.map((commentId) => {
						return <Comment key={commentId} id={commentId}></Comment>;
					})}
			</div>
		</div>
	);
}

export default Comment;
