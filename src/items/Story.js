import "../Items.css";
import {Link} from "react-router-dom";
import DateTime from "./DateTime.js";

function Story({story}) {
	return (
		<>
			<div className="story-title">
				{story.url !== undefined ? (
					<a href={story.url}>{story.title}</a>
				) : (
					<Link to={{pathname: "/item/" + story.id}}>{story.title}</Link>
				)}
			</div>
			<div className="story-details">
				{story.type !== "job" ? (
					<>
						{story.score} points by {story.by}{" "}
						<Link to={{pathname: "/item/" + story.id}}>
							<span>
								{" "}
								<DateTime seconds={story.time}></DateTime>{" "}
							</span>
						</Link>
						{story.descendants > 0 ? (
							<>
								|
								<Link to={{pathname: "/item/" + story.id}}>
									{" "}
									{story.descendants} {story.descendants > 1 ? "comments" : "comment"}
								</Link>
							</>
						) : null}
					</>
				) : null}
			</div>
		</>
	);
}

export default Story;
