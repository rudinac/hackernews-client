import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import axios from "axios";
import Story from "./Story.js";
import Comment from "./Comment.js";
import PollOptions from "./PollOptions.js";
import "../Items.css";
import DOMPurify from "dompurify";

async function fetchItemDetails(url) {
	const item = await axios(url);
	return item.data;
}

function StoryDetails(props) {
	let {id} = useParams();

	const [storyDetails, setStoryDetails] = useState(null);
	useEffect(() => {
		fetchItemDetails(
			`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`
		).then((fetchedDetails) => setStoryDetails(fetchedDetails));
	}, [id]);

	return (
		<>
			<div className="story-details-wrapper">
				{storyDetails && (
					<div className="item-details">
						<Story story={storyDetails}></Story>
					</div>
				)}
				{storyDetails && storyDetails.text && (
					<div
						className="story-text"
						dangerouslySetInnerHTML={{
							__html: DOMPurify.sanitize(storyDetails.text),
						}}
					></div>
				)}
				{storyDetails && storyDetails.parts && (
					<PollOptions partIds={storyDetails.parts} />
				)}
			</div>
			{storyDetails &&
				storyDetails.kids &&
				storyDetails.kids.map((commentId) => {
					return <Comment key={commentId} id={commentId} />;
				})}
		</>
	);
}

export default StoryDetails;
