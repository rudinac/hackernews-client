import {useEffect, useState} from "react";
import StoryList from "../StoryList.js";
import "../Items.css";
import React from "react";

const PAGE_SIZE = 30;
function HackerNews({typeOfStory}) {
	const [topStoryIds, setTopStoryIds] = useState([]);
	const [currentPage, setCurrentPage] = useState(1);

	useEffect(() => {
		fetch(
			`https://hacker-news.firebaseio.com/v0/${typeOfStory}.json?print=pretty`
		)
			.then((response) => response.json())
			.then((fetchedStoryIds) => setTopStoryIds(fetchedStoryIds));
	}, [typeOfStory]);

	const slicedStoryIds = topStoryIds.slice(
		(currentPage - 1) * PAGE_SIZE,
		currentPage * PAGE_SIZE
	);
	const numOfPages = parseInt(topStoryIds.length / PAGE_SIZE) + 1;
	function handleMore(event) {
		console.log(currentPage);
		setCurrentPage((currentPage) => currentPage + 1);
	}
	return (
		<>
			<StoryList
				key={currentPage}
				slicedStoryIds={slicedStoryIds}
				firstIndex={(currentPage - 1) * PAGE_SIZE + 1}
			></StoryList>
			{currentPage !== numOfPages ? (
				<div className="more" onClick={handleMore}>
					<span>More</span>
				</div>
			) : null}
		</>
	);
}
export default HackerNews;
