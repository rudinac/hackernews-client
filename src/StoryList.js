import {useState, useEffect} from "react";
import axios from "axios";
import Story from "./items/Story.js";
import "./Items.css";

async function fetchStoryDetails(url) {
	const story = await axios(url);
	return story.data;
}

function fetchStories(ids) {
	const requests = ids.map((id) =>
		fetchStoryDetails(
			`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`
		)
	);
	return Promise.all(requests);
}

function StoryList({slicedStoryIds, firstIndex}) {
	const [storiesToShow, setStoriesToShow] = useState([]);

	useEffect(() => {
		fetchStories(slicedStoryIds).then((fetchedStories) =>
			setStoriesToShow(fetchedStories)
		);
	}, [slicedStoryIds]);

	return (
		<div className="story-list">
			{storiesToShow.length > 0
				? storiesToShow.map((story, index) => {
						if (index === 1) console.log(story.id);
						return (
							<div key={story.id}>
								<div className="story-preview-container">
									<div className="ord">{index + firstIndex}.</div>
									<div>
										<Story story={story}></Story>
									</div>
								</div>
							</div>
						);
				  })
				: "Loading"}
		</div>
	);
}

export default StoryList;
